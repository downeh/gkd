const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const path = require('path');
const devMode = process.env.NODE_ENV !== 'production'
const CopyWebpackPlugin = require('copy-webpack-plugin');
var SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin');
// const PUBLIC_PATH = 'https://www.grantdownie.com/';
const PUBLIC_PATH = '/';
const WebpackPwaManifest = require('webpack-pwa-manifest');


module.exports = {
  entry: './src/index',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'app.min.js',
    publicPath: PUBLIC_PATH
  },
  devServer: {
    historyApiFallback: true,
  },
  resolve: {
    modules: [
      'node_modules',
      path.resolve(__dirname, 'src')
    ],
    extensions: ['.jsx', '.js', '.json', '.scss'],
  },

  module: {
    rules: [
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          devMode ? 'style-loader' : MiniCssExtractPlugin.loader,
          'css-loader',
          'postcss-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.jpe?g$|\.gif$|\.png$|\.svg$|\.woff$|\.ttf$|\.wav$|\.mp3$/,
        loader: 'file-loader',
        options: {
          name: 'images/[path][name].[ext]',
          context: './images'
        }
      },
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              "@babel/react",
              "@babel/preset-env",
              {
                plugins: [
                  '@babel/plugin-proposal-class-properties',
                  '@babel/transform-runtime'
                ]
              }
            ]
          },
        }
      }
    ]
  },

  plugins: [
    new HtmlWebpackPlugin({
      title: 'Setting up webpack 4',
      template: './src/index.html',
      inject: true,
      minify: {
        removeComments: true,
        collapseWhitespace: true
      },
    }),
    new MiniCssExtractPlugin({
      filename: devMode ? '[name].css' : '[name].min.css',
      chunkFilename: devMode ? '[id].css' : '[id].min.css',
    }),
    new CopyWebpackPlugin([
      {
        from: path.join(__dirname, "/src/assets/netlify/_redirects"),
        to: "_redirects",
        toType: "file"
      }
    ]),
    // new SWPrecacheWebpackPlugin(
    //   {
    //     cacheId: 'grantdownie',
    //     filename: 'service-worker.js',
    //     staticFileGlobs: ['dist/**/*.{js,css}', '/'],
    //     minify: true,
    //     navigateFallback: PUBLIC_PATH + 'index.html',
    //     stripPrefix: 'dist/',
    //     dontCacheBustUrlsMatching: /\.\w{6}\./
    //   }
    // ),
    new WebpackPwaManifest({
      name: 'GrantDownie',
      short_name: 'GD',
      description: 'GrantDownies personal development and portfolio home ',
      background_color: '#fafafa',
      theme_color: '#fafafa',
      'theme-color': '#fafafa',
      start_url: '/',
      icons: [
        {
          src: path.resolve('src/assets/icons/android-chrome-512x512.png'),
          sizes: [96, 128, 192, 256, 384, 512],
          destination: path.join('assets', 'icons')
        }
      ]
    })
  ]
}