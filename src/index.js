import React from 'react'
import ReactDOM from 'react-dom'
import AppContainer from 'containers/App'
import { BrowserRouter } from 'react-router-dom'

if (process.env.NODE_ENV !== 'production') {
  console.log('########################################')
  console.log('## Running in Development Mode ##');
  console.log('########################################')
}

ReactDOM.render(
  <BrowserRouter>
    <AppContainer />
  </BrowserRouter>,
  document.getElementById('root')
)