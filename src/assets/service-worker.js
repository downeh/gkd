console.log('I am a Service Worker!');

self.addEventListener('install', function(event) {
    // pre cache a load of stuff:
    event.waitUntil(
      caches.open('mycache').then(function(cache) {
        return cache.addAll([
          '/',
        ]);
      })
    )
  });


   self.addEventListener('fetch', function(event) {
    event.respondWith(caches.match(event.request));
  });

  self.addEventListener('fetch', function(event) {
    event.respondWith(
      caches.match(event.request).then(function(response) {
        return response || fetch(event.request);
      })
    );
  });