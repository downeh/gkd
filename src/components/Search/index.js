import React, { Component } from 'react'
import './styles.scss'
import Results from './ Results/index'
import TextField from '@material-ui/core/TextField';
import { searchPosts } from '../../api/wordpress'

export default class ResultsContainer extends React.Component {
  state = {
    open: true,
    results: []
  };

  async keyUpHandler(e) {
    let searchResult = [];
    let searchedTerm = e.target.value

    if (searchedTerm.length > 2) {
      searchResult = await searchPosts(searchedTerm)
    }
    else {
      searchResult = []
    }
    this.setState({ results: searchResult });
  }

  render() {
    return (
      <div style={{ textAlign: 'center' }}>
        <TextField
          id="search-input"
          type="search"
          margin="normal"
          autoFocus
          inputProps={{
            style: { textAlign: "center", fontSize: '2rem' }
          }}
          onKeyUp={(e) => this.keyUpHandler(e)}
        />
        <Results data={this.state.results} />
      </div>
    );
  }
}