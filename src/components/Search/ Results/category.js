import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import ListItemIcon from '@material-ui/core/ListItemIcon';
import PortfolioIcon from '@material-ui/icons/Favorite';
import TechnicalIcon from '@material-ui/icons/Code';
import PersonalIcon from '@material-ui/icons/Face';
import './styles.scss';

export default class CategoryIcon extends React.Component {
  constructor(props) {
    super(props);
    this.state = { category: '' }
  }

  resultItemType(category) {
    switch (category) {
      case 'Personal': //personal category
        return <PersonalIcon />
      case 'Technical': //technical category
        return <TechnicalIcon />
      case 'Portfolio': //portfolio category
        return <PortfolioIcon />
    }
  }

  componentWillMount() {
    if (this.props.category) {
      this.setState({ category: this.resultItemType(this.props.category) })
    } else {
      this.setState({ category: '' })
    }
  }

  render() {
    if (this.props.category.length > 1) {
      return (
        <ListItemIcon className="iconCategory">
          {this.state.category}
        </ListItemIcon>
      )
    } else {
      return (
        <></>
      )
    }
  }
}