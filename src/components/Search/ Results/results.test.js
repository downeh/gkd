import React from "react";
import { shallow, mount, render } from 'enzyme';
import { unwrap } from '@material-ui/core/test-utils'
import Results from './index';
import CategoryIcon from './category';

let UnwrappedComponent = unwrap(Results);

const dummyResult = [{
    slug: 'example-post',
    pageType: 'post',
    category: 'Personal',
    title: { rendered: 'Example Title' }
}]


describe('<Results />', () => {
    let wrapper;
    beforeAll(() => {
        wrapper = shallow(<UnwrappedComponent data={dummyResult} />);
    });

    test('I expect the result to contain the article title', async () => {
        const resultTitle = wrapper.find('.resultTitle').props();
        expect(resultTitle.primary).toEqual('Example Title')
    })

    test('I expect the result to have a category icon', async () => {
        const resultTitle = wrapper.find('.resultCategory').props();
        expect(resultTitle.category).toEqual('Personal')
    })
});

describe('<Category />', () => {
    let wrapper;

    beforeAll(() => {
        UnwrappedComponent = unwrap(CategoryIcon);
    });

    test('I expect no category provided to render', async () => {
        wrapper = shallow(<UnwrappedComponent category="" />);
        const iconCat = wrapper.find('.iconCategory');
        expect(iconCat.contains(<svg />))
    })

    test('I expect the category Personal to render', async () => {
        wrapper = shallow(<UnwrappedComponent category="Personal" />);
        const iconCat = wrapper.find('.iconCategory');
        expect(iconCat.contains(<svg />))
    })

    test('I expect the category Technical to render', async () => {
        wrapper = shallow(<UnwrappedComponent category="Technical" />);
        const iconCat = wrapper.find('.iconCategory');
        expect(iconCat.contains(<svg />))
    })

    test('I expect the category Portfolio to render', async () => {
        wrapper = shallow(<UnwrappedComponent category="Portfolio" />);
        const iconCat = wrapper.find('.iconCategory');
        expect(iconCat.contains(<svg />))
    })
});