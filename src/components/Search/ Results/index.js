import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { withRouter } from 'react-router';
import ResultItemType from './category';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import './styles.scss';


export default class Results extends React.Component {
  render() {
    const searchResults = this.props.data;

    if (searchResults.length < 1) {
      return (
        <p align="center">Feed me a word I may know</p>
      )
    } else {
      return (
        <div className='resultsContainer'>
          <List dense={true}>
            {searchResults.map(row => (
              <Link key={row.slug} to={`/${row.pageType}/${row.slug}`} style={{ textAlign: 'center' }}>
                <ListItem>
                  <ResultItemType
                    className="resultCategory"
                    category={row.category} />
                  <ListItemText
                    className="resultTitle"
                    primary={row.title.rendered}
                  />
                </ListItem>
              </Link>
            ))}
          </List>
        </div>
      );
    }
  }
}