import React from "react";
import { shallow, mount, render } from 'enzyme';
import { unwrap } from '@material-ui/core/test-utils'
import Search from './index';

describe('<Search />', () => {
    let wrapper;
    beforeAll(() => {
        wrapper = shallow(<Search />);
    });

    test('I expect the input to accept input and trigger keyUp event', async () => {
        expect(wrapper.find('#search-input').simulate("keyUp", { target: { value: "foo" } }))
    })

    test('I expect the input to do nothing if search term is less than 3 characters', async () => {
        expect(wrapper.find('#search-input').simulate("keyUp", { target: { value: "fo" } }))
    })

});