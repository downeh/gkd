import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import HomeIcon from '@material-ui/icons/HomeOutlined'
import './styles.scss'

class Logo extends Component {
  render() {
    return (
      <Link to={`/`} className="portfolioLink">
        <div className="homeIcon"><HomeIcon /></div>
        <div className="logoText">GrantDownie</div>
      </Link>
    )
  }
}

export default Logo
