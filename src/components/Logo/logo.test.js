import React from "react";
import { shallow, mount, render } from 'enzyme';
import LogoContainer from './index';


test('I expect the Logo home icon to show with an svg icon', async () => {
    const wrapper = shallow(<LogoContainer />);
    expect(wrapper.find('.homeIcon')).toExist();
    expect(wrapper.contains(<svg />))
})

test('I expect the logo text to match grantdownie', async () => {
    const wrapper = shallow(<LogoContainer />);
    const logoText = wrapper.find('.logoText').text()
    expect(logoText).toEqual('GrantDownie')
})

