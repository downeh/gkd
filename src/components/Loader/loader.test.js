import React from "react";
import { shallow, mount, render } from 'enzyme';
import FitnessLoader from './fitnessLoader';
import DefaultLoader from './index';

test('I expect the fitness loader to be visible', async () => {
    const wrapper = shallow(<DefaultLoader />);
    expect(wrapper.find('.loaderImg')).toExist();
})

test('I expect the fitness loader to be visible', async () => {
    const wrapper = shallow(<FitnessLoader />);
    expect(wrapper.find('.fitnessLoaderImg')).toExist();
})