import React, { Component } from 'react'
import spinnerImage from '../../assets/loader/svg-loaders/puff.svg'
import './styles.scss'

class Loader extends Component {
  render() {
    return (
      <div className="loaderContainer">
        <img className="loaderImg" src={spinnerImage} alt="Loading..." />
      </div>
    )
  }
}

export default Loader
