import React, { Component } from 'react'
import spinnerImage from '../../assets/loader/svg-loaders/audio.svg'
import './styles.scss'

class FitnessLoader extends Component {
  render() {
    return (
      <div><img className="fitnessLoaderImg" src={spinnerImage} alt="Loading..." /></div>
    )
  }
}

export default FitnessLoader
