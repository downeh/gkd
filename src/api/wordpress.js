const wpHost = 'https://grant-1a6bc8.easywp.com/wp-json/wp/v2/posts?_embed'
const wpCatHost = 'https://grant-1a6bc8.easywp.com/wp-json/wp/v2/categories'
const wpTagsHost = 'https://grant-1a6bc8.easywp.com/wp-json/wp/v2/tags/?per_page=100'

// technical id: 16
// personal id: 5
// portfolio id: 19
export const getTags = async () => {
  const response = await fetch(wpTagsHost)
  const json = await response.json()
  return json
}

const determineTags = async (postList) => {
  const tagList = await getTags
  const tagsList = await (tagList())

  //Posts
  await postList.forEach(async (postItem) => {
    //Post Item
    const postTagID = postItem.tags
    postItem.tagName = []
    postTagID.forEach((postTagID) => {
      //Tag ID
      let obj = tagsList.find(o => o.id === postTagID);
      if (obj) {
        postItem.tagName.push({ id: obj.id, name: obj.name })
      }
    })
  })
  return postList
}

export const getTechnicalPosts = async () => {
  const response = await (await fetch(`${wpHost}&categories=16`)).json()
  const postList = await determineTags(response)
  const final = await determineCategories(postList)
  return final
}

export const getCategories = async () => {
  const response = await fetch(wpCatHost)
  const json = await response.json()
  return json
}

export async function getPersonalPosts() {
  const response = await (await fetch(`${wpHost}&categories=5`)).json()
  const postList = await determineTags(response)
  const final = await determineCategories(postList)
  return final
}

export async function getPortfolioPosts() {
  const response = await fetch(`${wpHost}&categories=19`)
  const postList = await response.json()
  const postListModified = await determineTags(postList)
  const setFeaturedOrder = await setFeaturedItem(postListModified)
  const final = await determineCategories(setFeaturedOrder)
  return final
}

const setFeaturedItem = async (portfolioList) => {
  portfolioList.forEach(function (value, i) {
    value.featured = true;
  });
  return portfolioList;
}


export async function getPost(slug) {
  const response = await (await fetch(`${wpHost}&slug=${slug}`)).json()
  const post = await determineTags(response)
  return post
}

async function determineCategories(data) {
  data.forEach(function (value, i) {
    let cat = value.categories[0]

    switch (cat) {
      case 5: //personal category
        value.category = 'Personal'
        value.pageType = 'post'
        break;
      case 16: //technical category
        value.category = 'Technical'
        value.pageType = 'post'
        break;
      case 19: //portfolio category
        value.category = 'Portfolio'
        value.pageType = 'portfolio'
        break;
      default:
        value.category = 'Undefined'
        break;
    }
  });
  return data
}

export async function searchPosts(term) {
  const response = await (await fetch(`${wpHost}&search=${term}`)).json()
  const searchResults = await determineCategories(response)
  return searchResults
}
