import axios from 'axios';

let token = "70a70f95108f0a439c9a9472e23b245a3e2fe4ff"

export async function getAllTime() {
    let obtain = axios.all([
        axios.request("https://www.strava.com/api/v3/athletes/4047261/stats", {
            params: {
                access_token: token
            }
        })
    ]).then(axios.spread((statsData) => {
        const stats = statsData.data;
        let total = stats.all_run_totals.distance
        return total;
    }));
    return obtain;
}

export async function getThisWeek() {
    let obtain = axios.all([
        axios.request("https://www.strava.com/api/v3/athletes/4047261/stats", {
            params: {
                access_token: token
            }
        })
    ]).then(axios.spread((statsData) => {
        const stats = statsData.data;
        let total = stats.recent_run_totals.distance
        return total;
    }));
    return obtain;
}