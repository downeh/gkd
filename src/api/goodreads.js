var parseString = require('xml2js').parseString;

const apiKey = '6AtzFXc3P4VRPExvyFxew'

const options = {
    headers: new Headers({ 'content-type': 'application/json', 'origin': 'x-requested-with' }),
    mode: 'cors'
};

const corsOverride = "https://cors-anywhere.herokuapp.com/"

export const getWantToRead = async () => {
    let foundBooks;
    await (fetch(corsOverride + 'https://www.goodreads.com/review/list/94133152.xml?key=' + apiKey + '&v=2&shelf=to-read&per_page=200&page=1', options))
        .then(res => res.text())
        .then((body) => {
            parseString(body, function (err, result) {
                foundBooks = result.GoodreadsResponse.reviews[0].review;
            });
        });
    return foundBooks;
}

export const getRead = async () => {
    let foundBooks;
    await (fetch(corsOverride + 'https://www.goodreads.com/review/list/94133152.xml?key=' + apiKey + '&v=2&shelf=read&per_page=200&page=1', options))
        .then(res => res.text())
        .then((body) => {
            parseString(body, function (err, result) {
                foundBooks = result.GoodreadsResponse.reviews[0].review;
            });
        })
    return foundBooks;
}

export const getCurrentlyReading = async () => {
    let foundBooks;
    await (fetch(corsOverride + 'https://www.goodreads.com/review/list/94133152.xml?key=' + apiKey + '&v=2&shelf=currently-reading&per_page=200&page=1', options))
        .then(res => res.text())
        .then((body) => {
            parseString(body, function (err, result) {
                foundBooks = result.GoodreadsResponse.reviews[0].review;
            });
        });
    return foundBooks;
}