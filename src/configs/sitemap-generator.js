var sm = require('sitemap')
    , fs = require('fs');

var sitemap = sm.createSitemap({
    hostname: 'https://www.grantdownie.com',
    cacheTime: 600000,  //600 sec (10 min) cache purge period
    urls: [
        { url: '/', changefreq: 'weekly', priority: 0.8, lastmodrealtime: true },
        { url: '/technical', changefreq: 'weekly', priority: 0.8, lastmodrealtime: true },
        { url: '/personal', changefreq: 'weekly', priority: 0.8, lastmodrealtime: true },
        { url: '/portfolio', changefreq: 'weekly', priority: 0.8, lastmodrealtime: true },
        { url: '/fitness', changefreq: 'weekly', priority: 0.8, lastmodrealtime: true }
    ]
});

fs.writeFileSync("./dist/sitemap.xml", sitemap.toString());