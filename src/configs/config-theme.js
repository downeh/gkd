import { createMuiTheme } from '@material-ui/core/styles'
import { getStyles }      from 'core/libs/lib-style-helpers'

const colors = getStyles([
  'red',
  'hotPink',
  'aquaBlue',
  'transparent',
  'black'
])

const theme = createMuiTheme({
  typography: {
    useNextVariants: true
  },
  overrides: {
    MuiButton: { // Name of the component ⚛️ / style sheet
      text: { // Name of the rule
        color: 'red' // Some CSS
      }
    }
  },
  palette: {
    primary: {
      main: colors.transparent
    },
    secondary: {
      main: colors.black
    },
    error: {
      main: colors.red
    }
  }
})

export default theme
