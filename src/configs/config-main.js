/* Main app configs go here */

export const appConfig = {
  name: 'Grant Downie',
  shortName: 'GD',
  description: 'My home of personal development',
  splashScreenBackground: '#ffffff'
}
