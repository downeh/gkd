import './styles.scss'
import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import Hidden from '@material-ui/core/Hidden';
import FormatQuote from '@material-ui/icons/FormatQuote'
import { Typography } from '@material-ui/core'
import Grid from '@material-ui/core/Grid'
import Loader from '../../components/Loader'
import { getPersonalPosts, getTechnicalPosts, getPortfolioPosts } from '../../api/wordpress'

const moment = require('moment')

function Widget(props) {
  return <>
    <h3><span>{props.type}</span></h3><div className="widget">
      <h5><span><Link key={props.post.slug} to={`/${props.post.pageType}/${props.post.slug}`} className="mostRecent">{props.post.title.rendered}</Link></span></h5>
      <h6><span>{moment(props.post.date).format(props.dateFormat)}</span></h6>
    </div></>;
}

export default class Home extends Component {
  constructor(props) {
    super(props)
    this.state = { technicalPostArr: [], personalPostArr: [], portfolioPostArr: [] }
  }

  componentWillMount() {
    this.postRender();
  }

  async postRender() {
    const technicalPost = await getTechnicalPosts()
    const personalPost = await getPersonalPosts()
    const portfolioPost = await getPortfolioPosts()
    await (this.setState({ technicalPostArr: technicalPost, personalPostArr: personalPost, portfolioPostArr: portfolioPost }))
  }

  render() {
    const technicalPost = this.state.technicalPostArr;
    const personalPost = this.state.personalPostArr;
    const portfolioPost = this.state.portfolioPostArr;

    if (technicalPost.length < 1) {
      return (<Loader />)
    }
    return (
      <Grid container direction="row" justify="center" alignItems="center">
        <Hidden smDown>
          <Grid item lg={6} xs={12} className="welcomeText">
            <div className="homeWho"><h1>It's Grant!</h1></div>
            <Typography component="h3" variant="h3" align="left"><FormatQuote className="icon left" /></Typography>
            <Typography component="h3" variant="h3" align="center" className="tagline">
              <span>...a frontend</span> <b> React </b> <span>developer that enjoys engineering <b>visually </b> <u>performant</u> things.</span>
            </Typography>
            <Typography component="h3" variant="h3" align="right"><FormatQuote className="icon right" /></Typography>
          </Grid>
        </Hidden>
        <Grid item lg={6} xs={12}>
          <div className="myPic">
            <div className="widgetContainer">
              <Widget type="Latest Technical" post={technicalPost[0]} dateFormat="MMMM Do YYYY" /><br />
              <Widget type="Latest Personal" post={personalPost[0]} dateFormat="MMMM Do YYYY" /><br />
              <Widget type="Latest Project" post={portfolioPost[0]} dateFormat="MMMM YYYY" /><br />
            </div>
          </div>
        </Grid>
      </Grid>
    )
  }
}