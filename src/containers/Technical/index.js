import './styles.scss'
import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableHead from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableRow from '@material-ui/core/TableRow'
import Chip from '@material-ui/core/Chip'
import { getTechnicalPosts } from '../../api/wordpress'
import Loader from '../../components/Loader'

const moment = require('moment')

class Technical extends Component {
  constructor(props) {
    super(props)
    this.state = { postList: [] }
  }

  componentWillMount() {
    this.postRender()
  }

  async postRender() {
    const posts = await getTechnicalPosts()
    await (this.setState({ postList: posts }))
  }

  render() {
    const data = this.state.postList

    if (data.length < 1) {
      return (<Loader />)
    }
    return (
      <div id="technicalPage">
        <div className="technicalHeader">TECHNICAL ACTIVITY</div>
        <Table>
          <TableHead>
            <TableRow className="tableHeader">
              <TableCell colSpan={5}>Title</TableCell>
              <TableCell colSpan={3}>Tags</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map(row => (
              <TableRow key={row.id} className="postRow">
                <TableCell colSpan={5}><Link key={row.slug} to={`/post/${row.slug}`} className="technicalTablCell_Link">{row.title.rendered}</Link></TableCell>
                <TableCell colSpan={3}>
                  {row.tagName.map(tag => <Chip color="default" variant="outlined" key={tag.id} label={tag.name} />)}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </div>
    )
  }
}

export default Technical
