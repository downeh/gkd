import './styles.scss'
import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import PropTypes from 'prop-types'
import { Typography } from '@material-ui/core'
import Grid from '@material-ui/core/Grid';
import Loader from '../../components/Loader'
import Chip from '@material-ui/core/Chip'
import { getPost } from '../../api/wordpress'

import Prism from "prismjs";
import "../../../node_modules/prismjs/themes/prism-coy.css";

const moment = require('moment')

class Post extends Component {
  constructor(props) {
    super(props)
    this.state = { post: [] }
  }

  componentDidMount() {
    Prism.highlightAll();
  }

  componentWillMount() {
    this.postRender(this.props.match.params.postslug)
  }

  async postRender(slug) {
    const postResult = await getPost(slug)
    this.setState({ post: postResult })
    const postBody = document.getElementById('postBody')
    postBody.innerHTML = postResult[0].content.rendered
  }

  render() {
    const data = this.state.post

    if (data.length < 1) {
      return (
        <Loader />
      )
    }
    return (
      <div>
        {data.map(postItem => (
          <div key={postItem.slug}>
            <Grid container>
              <Grid item xs={6}>
                <div className="postTags">
                  {postItem.tagName.map(tag => <Chip color="default" variant="outlined" key={tag.id} label={tag.name} />)}
                </div>
              </Grid>
              <Grid item xs={6}>
                <div className="postDate"><Typography component="h4" variant="h4" align="right">{moment(postItem.date).format('MMMM Do YYYY')}</Typography></div>
              </Grid>
            </Grid>

            <div className="postBackground" style={{ background: `url(${postItem._embedded['wp:featuredmedia']['0'].source_url})` }}>
              <Grid container>
                <Grid item xs={12}>
                  <div className="postTitle"><span>{postItem.title.rendered}</span></div>
                </Grid>
              </Grid>
              <div className="postContent">
                <div id="postBody" className="postBodyText" />
              </div>
            </div>
          </div>
        ))}
      </div>
    )
  }
}

// Post.propTypes = {
//   slug: PropTypes.string.isRequired
// }


export default withRouter(Post)
