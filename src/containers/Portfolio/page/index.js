import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Typography } from '@material-ui/core'
import './styles.scss'
import { getPost } from '../../../api/wordpress'
import Loader from '../../../components/Loader'
import { withRouter } from 'react-router-dom'
import Chip from '@material-ui/core/Chip'

const moment = require('moment')

class PortfolioItem extends Component {
  constructor(props) {
    super(props)
    this.state = { post: [] }
  }

  componentWillMount() {
    this.postRender(this.props.match.params.portfolioslug)
  }

  async postRender(slug) {
    const postResult = await getPost(slug)
    this.setState({ post: postResult })
    const postBody = document.getElementById('pageBody')
    postBody.innerHTML = postResult[0].content.rendered
  }

  render() {
    const data = this.state.post

    if (data.length < 1) {
      return (
        <Loader />
      )
    }
    return (
      <div>
        {data.map(postItem => (
          <div key={postItem.slug}>
            <div className="pageDate"><Typography component="h4" variant="h4" align="right">{moment(postItem.date).format('MMMM YYYY')}</Typography></div>
            <div className="pageTags" align="right">
              {postItem.tagName.map(tag => <Chip color="default" variant="outlined" key={tag.id} label={tag.name} />)}
            </div>
            <div className="pageBackground" style={{ background: `url(${postItem._embedded['wp:featuredmedia']['0'].source_url})` }}>
              <div className="pageTitle"><span>{postItem.title.rendered}</span></div>
              <div className="pageContent">
                <div id="pageBody" className="pageBodyText" />
              </div>
            </div>
          </div>
        ))}
      </div>
    )
  }
}

// Post.propTypes = {
//   slug: PropTypes.string.isRequired
// }


export default withRouter(PortfolioItem)
