import './styles.scss'
import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import Favorite from '@material-ui/icons/Favorite';
import Loader from '../../components/Loader'
import { getPortfolioPosts } from '../../api/wordpress';

const moment = require('moment')

class Portfolio extends Component {
  constructor(props) {
    super(props)
    this.state = { portfolioList: [] }
  }

  componentWillMount() {
    this.populatePortfolioList();
  }

  async populatePortfolioList() {
    const posts = await getPortfolioPosts()
    await (this.setState({ portfolioList: posts }))
  }

  render() {
    const data = this.state.portfolioList
    if (data < 1) {
      return (
        <Loader />
      )
    }

    else {
      return (
        <div id="portfolioPage">
          <div className="portfolioHeader"><Favorite className="porfolioFavIcon" /> PORTFOLIO</div>
          <GridList cellHeight={200} spacing={50}>
            {data.map(tile => (
              <GridListTile key={tile.slug} cols={tile.featured ? 2 : 1} rows={tile.featured ? 2 : 1}>
                <img src={tile._embedded['wp:featuredmedia']['0'].source_url} alt={tile.title.rendered} />
                <Link key={tile.slug} to={`/portfolio/${tile.slug}`} className="portfolioLink">
                  <GridListTileBar
                    className="portfolioTile"
                    title={tile.title.rendered}
                    titlePosition="top"
                  />
                  <GridListTileBar
                    className="portfolioTile"
                    title={moment(tile.date).format('MMMM YYYY')}
                    titlePosition="bottom"
                  />
                </Link>
              </GridListTile>
            ))}
          </GridList>
        </div>
      )
    }

  }
}

export default Portfolio
