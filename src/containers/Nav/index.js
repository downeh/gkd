import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import AppBar from '@material-ui/core/AppBar'
import CssBaseline from '@material-ui/core/CssBaseline'
import Drawer from '@material-ui/core/Drawer'
import IconButton from '@material-ui/core/IconButton'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import MenuIcon from '@material-ui/icons/Menu'
import Toolbar from '@material-ui/core/Toolbar'
import SearchIcon from '@material-ui/icons/SearchOutlined';
import './styles.scss'
import Logo from '../../components/Logo'

const drawerWidth = 275

const sideBarItems = [
  { section: 'Welcome', nav: '' },
  { section: 'Personal', nav: 'personal' },
  { section: 'Technical', nav: 'technical' },
  { section: 'Portfolio', nav: 'portfolio' },
  { section: 'Fitness', nav: 'fitness' },
  { section: 'Reading List', nav: 'books' }
]

class Nav extends Component {
  state = {
    mobileOpen: false
  };

  openResume(e) {
    e.preventDefault();
    window.open('https://www.gkd.design/', '_blank')
    this.handleDrawerToggle()
  }

  handleDrawerToggle = () => {
    this.setState(state => ({ mobileOpen: !state.mobileOpen }))
  };

  render() {
    const drawer = (
      <div className="sideMenu" style={{ width: drawerWidth }}>
        <List>
          {sideBarItems.map(text => (
            <Link key={text.section} to={`/${text.nav}`} className="navLink" onClick={() => this.handleDrawerToggle()}>
              <ListItem button>
                <ListItemText disableTypography primary={text.section} />
              </ListItem>
            </Link>
          ))}
          <ListItem button className="navLink">
            <ListItemText disableTypography primary={"Resume"} onClick={(e) => this.openResume(e)} />
          </ListItem>
        </List>
      </div>
    )

    return (
      <div>
        <CssBaseline />
        <AppBar position="static" className="appBarOverride">
          <Toolbar variant="dense">
            <IconButton
              style={{ zIndex: 1 }}
              position="fixed"
              align="right"
              anchor="left"
              color="secondary"
              aria-label="Open drawer"
              onClick={this.handleDrawerToggle}
            >
              <MenuIcon />
            </IconButton>
            <Logo />
            <div className="grow" />
            <Link to={`/search`} className="mostRecent">
              <SearchIcon onClick={this.handleOpen} className="searchIcon" />
            </Link>
          </Toolbar>
        </AppBar>
        <nav>
          <Drawer
            variant="temporary"
            anchor="left"
            open={this.state.mobileOpen}
            onClose={this.handleDrawerToggle}
          >
            {drawer}
          </Drawer>
        </nav>
      </div>
    )
  }
}

// Nav.propTypes = {
//   callback: PropTypes.func.isRequired
// }

export default Nav
