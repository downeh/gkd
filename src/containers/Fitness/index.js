import React, { Component } from 'react'
import './styles.scss'
import Grid from '@material-ui/core/Grid';
import RunIcon from '@material-ui/icons/DirectionsRun'
import Fab from '@material-ui/core/Fab';
import Button from '@material-ui/core/Button';
import FitnessLoader from '../../components/Loader/fitnessLoader'
import { getAllTime, getThisWeek } from '../../api/strava';

const filterItemsUnit = [
  { id: 'metres', display: 'Metres', active: true, type: 'Unit' },
  { id: 'miles', display: 'Miles', active: false, type: 'Unit' },
  { id: 'kilometres', display: 'Kilometres', active: false, type: 'Unit' }
]

const filterItemsTime = [
  { id: 'all', display: 'All', active: true, type: 'Time' },
  { id: 'recent', display: 'Recent', active: false, type: 'Time' }
]

function Metric(props) {
  const metricReady = props.metricReady;
  if (metricReady) {
    return <div>{props.metricValue} <p>{props.metricUnit}</p></div>;
  }
  return <FitnessLoader />;
}

function convertUnits(unitType, receivedValue) {
  let conversion, rounded;
  if (unitType == 'miles') {
    conversion = receivedValue * 0.00062137;
    rounded = Math.round(conversion * 10) / 10;
  } else if (unitType == 'kilometres') {
    conversion = receivedValue / 1000
    rounded = Math.round(conversion * 10) / 10;
  } else if (unitType == 'metres') {
    rounded = Math.round(receivedValue * 10) / 10;
  }
  return rounded;
}

class Fitness extends Component {
  constructor(props) {
    super(props)
    this.state = {
      filterUnits: filterItemsUnit,
      filterTime: filterItemsTime,
      metric: false,
      metricRaw: null,
      metricValue: null,
      metricUnit: "metres",
      timePeriod: "all"
    }
  }

  componentWillMount() {
    setTimeout(() => {
      this.obtainData(this.state.timePeriod)
    }, 4000)
  }

  async obtainData(TimePeriod) {
    let returnedMetric, finalMetric;
    if (TimePeriod == 'all') {
      // returnedMetric = await (getAllTime());
      returnedMetric = 163400;
    } else {
      // returnedMetric = await (getThisWeek());
      returnedMetric = 20000;
    }
    finalMetric = convertUnits(this.state.metricUnit, returnedMetric)
    this.setState({ metric: true, metricRaw: returnedMetric, metricValue: finalMetric });
  }

  onMetricClick(filterItem) {
    let adjustedMetric;
    switch (filterItem) {
      case 'metres':
        adjustedMetric = convertUnits(filterItem, this.state.metricRaw)
        filterItemsUnit[0].active = true;
        filterItemsUnit[1].active = false;
        filterItemsUnit[2].active = false;
        break;
      case 'miles':
        adjustedMetric = convertUnits(filterItem, this.state.metricRaw)
        filterItemsUnit[0].active = false;
        filterItemsUnit[1].active = true;
        filterItemsUnit[2].active = false;
        break;
      case 'kilometres':
        adjustedMetric = convertUnits(filterItem, this.state.metricRaw)
        filterItemsUnit[0].active = false;
        filterItemsUnit[1].active = false;
        filterItemsUnit[2].active = true;
        break;
    }
    this.setState({ filterUnits: filterItemsUnit, metricUnit: filterItem, metricValue: adjustedMetric })
  }

  async onTimeClick(timePeriod) {
    let recent;
    switch (timePeriod) {
      case 'all':
        filterItemsTime[0].active = true;
        filterItemsTime[1].active = false;
        recent = this.obtainData(timePeriod);
        break;
      case 'recent':
        filterItemsTime[0].active = false;
        filterItemsTime[1].active = true;
        recent = this.obtainData(timePeriod);
        break;
    }
    this.setState({ filterUnits: filterItemsUnit, metricValueRaw: recent, metric: true, timePeriod: timePeriod })
  }

  render() {
    return (
      <div className="banner">
        <Grid item lg={12} xs={12}>
          <div className="fitnessCaption">
            <p align="center">I like to run. I tell Strava all about it. Let me tell you about it via their API.</p>
          </div>
        </Grid>
        <Grid container>
          <Grid item lg={6} xs={12}>
            <div id="fitnessMetric">
              <Metric metricReady={this.state.metric} metricValue={this.state.metricValue} metricUnit={this.state.metricUnit} />
            </div>
          </Grid>
          <Grid item lg={6} xs={12} align="center">
            <div className="fitnessSubheading">Units</div>
            <div id="fitnessMetricChooser">
              {this.state.filterUnits.map((item, i) =>
                <Button key={item.id} variant="outlined"
                  className={item.active ? "activeMetric" : "metricChooserBtn"}
                  onClick={() => this.onMetricClick(item.id, item.type)}>{item.display}
                </Button>
              )}
              <div className="fitnessSubheading">Time</div>
              {this.state.filterTime.map((item, i) =>
                <Button key={item.id} variant="outlined"
                  className={item.active ? "activeMetric" : "metricChooserBtn"}
                  onClick={() => this.onTimeClick(item.id, item.type)}>{item.display}
                </Button>
              )}
            </div>
          </Grid>
        </Grid>
      </div>
    )
  }
}

export default Fitness