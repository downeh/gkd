import React, { Component } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { MuiThemeProvider } from '@material-ui/core/styles'
import theme from 'configs/config-theme'
import Nav from 'containers/Nav'
import Welcome from 'containers/Home'
import Technical from 'containers/Technical'
import Personal from 'containers/Personal'
import Post from 'containers/Post'
import Portfolio from 'containers/Portfolio'
import PortfolioItem from 'containers/Portfolio/page'
import Search from '../../components/Search';
import Fitness from '../../containers/Fitness';
import Books from '../../containers/Books';
import './styles.scss'

class App extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <Nav />
        <Switch>
          <Route exact path="/" render={() => <Welcome />} />
          <Route path="/technical" render={() => <Technical />} />
          <Route path="/search" render={() => <Search />} />
          <Route path="/personal" render={() => <Personal />} />
          <Route path="/fitness" render={() => <Fitness />} />
          <Route path="/books" render={() => <Books />} />
          <Route path="/post/:postslug" render={() => <Post />} />
          <Route exact path="/portfolio" render={() => <Portfolio />} />
          <Route path="/portfolio/:portfolioslug" render={() => <PortfolioItem />} />
        </Switch>
      </MuiThemeProvider>
    )
  }
}

export default App
