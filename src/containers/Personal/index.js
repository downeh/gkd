import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Typography } from '@material-ui/core'
import GridList from '@material-ui/core/GridList'
import GridListTile from '@material-ui/core/GridListTile'
import './styles.scss'
import Loader from '../../components/Loader'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'

import { getPersonalPosts } from '../../api/wordpress'

const classes = () => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: 'white'
  },
  gridList: {
    width: 400,
    height: 500,
    transform: 'translateZ(0)'
  }
})

class Personal extends Component {
  constructor(props) {
    super(props)
    this.state = { postList: [] }
  }

  componentWillMount() {
    this.postRender()
  }

  async postRender() {
    const posts = await getPersonalPosts()
    await (this.setState({ postList: posts }))
  }

  render() {
    const data = this.state.postList

    if (data.length < 1) {
      return (<Loader />)
    }
    return (
      <div classes={classes.root} style={{ padding: '3vw' }}>
        <div className="personalHeader">PERSONAL ACTIVITY</div>
        <GridList spacing={10}>
          {data.map(row => (
            <GridListTile key={row.id} style={{ cursor: 'pointer' }}>
              <img src={row._embedded['wp:featuredmedia']['0'].source_url} alt="pic" />
              <div className="customTileTitle">
                <Link key={row.slug} to={`/post/${row.slug}`} style={{ textDecoration: 'none', color: 'black' }}>
                  <span>{row.title.rendered}</span></Link>
              </div>
            </GridListTile>
          ))}
        </GridList>
      </div>
    )
  }
}

export default Personal
