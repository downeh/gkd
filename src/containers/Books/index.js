import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Typography } from '@material-ui/core'
import './styles.scss'
import Loader from '../../components/Loader'
import SwipeableViews from 'react-swipeable-views';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import BookSection from './bookSections';

import { getRead, getWantToRead, getCurrentlyReading } from '../../api/goodreads'

function TabContainer(props) {
  const { children, dir } = props;

  return (
    <Typography component="div" dir={dir} style={{ padding: 1 * 3 }}>
      {children}
    </Typography>
  );
}

const styles = theme => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    width: '100%',
    position: 'relative',
    height: 'auto',
  }
});

class Books extends Component {
  constructor(props) {
    super(props)
    this.state = { readBooks: [], value: 0, loading: true }
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleChangeIndex = index => {
    this.setState({ value: index });
  };

  async componentWillMount() {
    let obtainBooks = await (this.getBooks())
    await (obtainBooks)
    this.setState({ booksReading: obtainBooks.booksCurrentlyReading, booksToRead: obtainBooks.booksToRead, booksRead: obtainBooks.booksRead, loading: false })
  }

  async getBooks() {
    const booksCurrentlyReading = await getCurrentlyReading()
    const booksToRead = await getWantToRead()
    const booksRead = await getRead()
    let booksArr = { booksCurrentlyReading, booksToRead, booksRead }
    return (booksArr)
  }

  render() {
    const { classes, theme } = this.props;

    if (this.state.loading === true) {
      return (<Loader />)
    }

    else {
      return (
        <>
          <div className="personalHeader" style={{ paddingBottom: '4rem' }}>My Book Shelf</div>
          <div className={classes.root}>
            <AppBar position="static" color="default">
              <Tabs
                value={this.state.value}
                onChange={this.handleChange}
                indicatorColor="primary"
                textColor="secondary"
                variant="fullWidth"
              >
                <Tab label="Currently Reading" />
                <Tab label="Read" />
                <Tab label="Want To Read" />
              </Tabs>
            </AppBar>
            <SwipeableViews
              axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
              index={this.state.value}
              onChangeIndex={this.handleChangeIndex}
            >
              <TabContainer dir={theme.direction}><BookSection books={this.state.booksReading} /></TabContainer>
              <TabContainer dir={theme.direction}><BookSection books={this.state.booksRead} /></TabContainer>
              <TabContainer dir={theme.direction}><BookSection books={this.state.booksToRead} /></TabContainer>
            </SwipeableViews>
          </div>
        </>
      )
    }

  }
}

export default withStyles(styles, { withTheme: true })(Books);

