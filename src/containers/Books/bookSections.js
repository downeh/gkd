import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import image from '../../assets/images/1.jpg';

const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        overflow: 'hidden'
    },
    icon: {
        color: 'rgba(255, 255, 255, 0.54)',
    },
});


class BookSection extends Component {
    constructor(props) {
        super(props)
        this.state = { bookArr: [] }
    }

    componentWillMount() {
        this.sortBooks(this.props.books)
    }

    sortBooks(books) {
        let bookArr = []

        books.forEach(function (element) {
            let bookObj = {};
            bookObj.title = element.book[0].title[0]
            bookObj.author = element.book[0].authors[0].author[0].name[0]
            bookObj.img = element.book[0].small_image_url[0]
            bookObj.started = element.started_at[0]
            bookObj.updated = element.date_updated[0]
            bookObj.link = element.link[0]
            bookArr.push(bookObj)
        });
        this.setState({ books: bookArr })
    }
    render() {
        let bookArr = this.state.books
        return (
            <div className={styles.root}>
                <GridList cellWidth={"100%"} cellHeight={30} className={styles.gridList}>
                    {bookArr.map(book => (
                        <GridListTile key={book.title} cols={2} rows={2}>
                            <GridListTileBar
                                title={<label><a style={{ color: 'white', fontWeight: 300 }} href={book.link} target="_blank"> {book.title}</a></label>}
                                subtitle={<span>{book.author}</span>}
                            />
                        </GridListTile>
                    ))}
                </GridList>
            </div>
        );
    }
}

export default withStyles(styles)(BookSection);